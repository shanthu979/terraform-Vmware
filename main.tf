# #VMware vSphere Provider
terraform {
  required_version = ">= 0.13"
}
terraform {
  required_providers {
    vsphere = {
      source  = "hashicorp/vsphere"
      version = "2.4.1"
    }
  }
}
terraform {
  backend "http" {
  }
}


provider "vsphere" {
  #Set of variables used to connect to the vCenter
  vsphere_server = var.vsphere_server
  user           = var.vsphere_user
  password       = var.vsphere_password

  #If you have a self-signed cert
  allow_unverified_ssl = true
}

#Name of the Datacenter in the vCenter
data "vsphere_datacenter" "dc" {
  name = "bootlabs"
}
#Name of the Cluster in the vCenter
data "vsphere_compute_cluster" "cluster" {
  name          = "Sailor"
  datacenter_id = data.vsphere_datacenter.dc.id
}
#Name of the Datastore in the vCenter, where VM will be deployed
data "vsphere_datastore" "datastore" {
  name          = "datastore1 (1)"
  datacenter_id = data.vsphere_datacenter.dc.id
}
#Name of the Portgroup in the vCenter, to which VM will be attached
data "vsphere_network" "network" {
  name          = "VM Network"
  datacenter_id = data.vsphere_datacenter.dc.id
}
#Name of the Templete in the vCenter, which will be used to the deployment
data "vsphere_virtual_machine" "template" {
  name          = "ubu"
  datacenter_id = data.vsphere_datacenter.dc.id
}

# Set VM parameteres
resource "vsphere_virtual_machine" "ubu-testing" {
  name             = "ubu-test"
  num_cpus         = 3
  memory           = 4096
  guest_id         = "ubuntu64Guest"
  resource_pool_id = data.vsphere_compute_cluster.cluster.resource_pool_id
  datastore_id     = data.vsphere_datastore.datastore.id

  network_interface {
    network_id = data.vsphere_network.network.id
  }


  disk {
    label            = "disk0"
    thin_provisioned = false
    size             = 30
  }


  clone {
    template_uuid = data.vsphere_virtual_machine.template.id
  }
}
output "VM_Name" {
  value = vsphere_virtual_machine.ubu-testing.name
}

output "VM_IP_Address" {
  value = vsphere_virtual_machine.ubu-testing.guest_ip_addresses
}
